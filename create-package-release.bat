@echo off

REM Paths
set nugetpath=".\\.nuget\\NuGet.exe"
set projectpath=".\\Atlassian.Jira\\Atlassian.Jira.csproj"
set outputdirectory=".\\"

REM Clean up artifacts
echo ==========
echo Removing artifacts from previous builds...
@rd "..\Atlassian.Jira\bin" /S /Q
@rd "..\Atlassian.Jira\obj" /S /Q
echo Done.
echo ==========

REM Prep output location
echo.
echo.
echo ==========
echo Creating output directory...
if not exist %outputdirectory% md %outputdirectory%
echo Done.
echo ==========

REM Create package
echo.
echo.
echo ==========
echo Building and packaging...
"%nugetpath%" pack "%projectpath%" -Build -OutputDirectory "%outputdirectory%" -Properties Configuration=Release
echo Done.
echo ==========
echo.
@pause
@echo off

REM Paths
set nugetpath=".\\.nuget\\NuGet.exe"
set projectpath=".\\Atlassian.Jira\\Atlassian.Jira.csproj"
set outputdirectory=".\\"

REM Clean up artifacts
echo ==========
echo Removing artifacts from previous builds...
@rd "..\Atlassian.Jira\bin" /S /Q
@rd "..\Atlassian.Jira\obj" /S /Q
echo Done.
echo ==========

REM Prep output location
echo.
echo.
echo ==========
echo Creating output directory...
if not exist %outputdirectory% md %outputdirectory%
echo Done.
echo ==========

REM Generate version number
set datestamp=%date:~10,4%%date:~4,2%%date:~7,2%
set datestamp=%datestamp: =0%
set timestamp=%time::=%
set timestamp=%timestamp:~0,6%
set timestamp=%timestamp: =0%
set version=0.0.0-pre-%datestamp%-%timestamp%

REM Create package
echo.
echo.
echo ==========
echo Building and packaging...
"%nugetpath%" pack "%projectpath%" -Build -Version %version% -OutputDirectory %outputdirectory% -Properties Configuration=Release
echo Done.
echo ==========
echo.
@pause